import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyDB756_bHSXe4IkODQBP9VyZKQ5s-kWb30",
    authDomain: "keithjokes-7b3e1.firebaseapp.com",
    databaseURL: "https://keithjokes-7b3e1.firebaseio.com",
    projectId: "keithjokes-7b3e1",
    storageBucket: "keithjokes-7b3e1.appspot.com",
    messagingSenderId: "83218528537",
    appId: "1:83218528537:web:c5cf4c48953475a8cfed88",
    measurementId: "G-Y2M6YZPZQS"
};

// Initialize Firebase
firebase.initializeApp(config);

export default firebase;