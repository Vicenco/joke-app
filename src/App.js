import React from 'react';
import JokeList from './components/JokeList/JokeList.comp';
import './App.scss';

function App() {
  return (
    <div className="App">
      <div className="container">
        <JokeList />
      </div>
      <footer className="noselect">
        <span>&copy; Vicenco 2020</span>
      </footer>
    </div>
  );
}

export default App;
