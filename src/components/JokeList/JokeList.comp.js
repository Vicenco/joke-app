import React, { useState, useEffect } from 'react';
import firebase from '../../firebase/config';

import Joke from '../Joke/Joke.comp';
import { 
    appTitle, 
    duplicateText, 
    loadingText, 
    mainEmoji, 
    buttonLabel, 
    versionText, 
    versionNum } from '../../configuration';

import './jokeList.scss';

function JokeList() {

    const [loading, setLoading] = useState(false);
    const [jokes, setJokes] = useState(JSON.parse(window.localStorage.getItem("jokes") || "[]"));
    const [hasError, setHasError] = useState({ message: "", error: false });
    const firebaseDB = firebase.firestore();

    let defaultProps = { numJokesToGet: 10 }
    let seenJokes = new Set();
    let aSortedJokes = jokes.sort((a, b) => b.votes - a.votes);


    /**
     * Replaces componentDidMount / componentDidUpdate.
     * @protected
     */
    useEffect(() => {

        if (jokes.length === 0) {
            getJokes();
        } else {
            jokes.map(j => seenJokes.add(j.joke));
        }

    });


    /**
     * Obtain jokes from api service.
     * @private
     */
    async function getJokes() {

        try {

            // Load jokes.
            let jokes = [];
            if (!hasError.error){
                var i = 0;
                while ( i < defaultProps.numJokesToGet) {

                    const res = await firebaseDB.collection("jokes").get();
                    let aJokes = res.docs.map(joke => ({
                        ...joke.data(),
                        id: joke.id
                    }));

                    for (let idx = 0; idx < aJokes.length; idx++) {

                        const oItem = aJokes[idx];
    
                        // If joke is currently not in the set, add it to the array.
                        if (!seenJokes.has(oItem.joke)) {
                            jokes.push({ id: oItem.id, joke: oItem.joke, votes: oItem.votes });
                            i++;
                        } else {
        
                            // Log found duplicates.
                            console.log(duplicateText);
                            i++;                            
        
                        }        

                    };
    
                }

            }
            
            if (jokes.length > 0){

                // Add jokes to array set.
                setJokes(prevState => {
                    return [...prevState, ...jokes]
                });
                
            }

            setLoading(false);
            setStorage();

        }
        catch (oError) {

            // Catch all error warnings.
            setHasError({ message: oError.message, error: true });
            setLoading(false);

        }

    };


    /**
     * Handles voting up/down.
     * 
     * @desc Connects to firebase db to update the vote of a single joke.
     * @param {string} id - Id of joke. 
     * @param {integer} delta - Value to up/down vote.
     * @public
     */
    const handleVote = (id, delta) => {
        
        setJokes(prevState => prevState.map(j => j.id === id ? { ...j, votes: j.votes + delta } : j ));        
        jokes.map(j => j.id === id ? firebaseDB.collection("jokes").doc(id).update({votes: j.votes + delta }) : null );

    };


    /**
     * Obtains new jokes on user press.
     * @public
     */
    const handleGetJokes = () => {

        setLoading(true);
        getJokes();

    };


    /**
     * Sets data to the localstorage.
     * @private
     */
    const setStorage = () => {
        window.localStorage.setItem("jokes", JSON.stringify(jokes));
    };

    let openClass = "";
    if (aSortedJokes.length > 0){
        openClass = "jokeList-jokes displayJokes"
    } else {        
        openClass = "jokeList-jokes"
    }

    return (

        <div className="jokeList noselect">
            <div className="jokeList-sidebar">
                <div className="wrapper">
                    <h1 className="title" dangerouslySetInnerHTML={{ __html: appTitle }} />
                    <img src={mainEmoji} alt="Logo" />
                </div>
                <button className="getMoreBtn" onClick={ () => handleGetJokes() }>{buttonLabel}</button>
                <div className="version">
                    <span>{versionText} {versionNum}</span>
                </div>
                <div className="overlay" />
            </div>
            <div className="jokeList-column">
                <div className={openClass} >
                    {
                        aSortedJokes.map(j => (
                            <Joke
                                key={j.id}
                                votes={j.votes}
                                text={j.joke}
                                upVote={ () => handleVote(j.id, 1 )}
                                downVote={ () => handleVote(j.id, -1) }
                            />
                        ))
                    }
                </div>
                {
                    hasError.error ? (
                        <div className="errorMessage">
                            <h2>Error message</h2>
                            <span>{hasError.message}</span>
                        </div>
                    ) : null
                }
                {
                    loading ? (
                        <div className="spinner">
                            <h2 className="loadText">{loadingText}</h2>
                        </div>
                    ) : null
                }
            </div>
        </div>
    );

}

export default JokeList;