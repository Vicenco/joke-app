import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons';
import './joke.scss';

const Joke = ({ upVote, downVote, text, votes }) => {

    const [show, setShow] = useState(false);

    const getColor = () => {

        if (votes >= 15){
            return "#4CAF50";
        } else if (votes >= 12){
            return "#8BC34A";
        } else if (votes >= 9){
            return "#CDDC39";
        } else if (votes >= 6){
            return "#FFEB3B";
        } else if (votes >= 3){
            return "#FFC107";
        } else if (votes >= 0){
            return "#FF9800";
        } else {
            return "#f44336";
        }

    };

    const getEmoji = () => {

        if (votes >= 15){
            return "em em-rolling_on_the_floor_laughing";
        } else if (votes >= 12){
            return "em em-laughing";
        } else if (votes >= 9){
            return "em em-smiley";
        } else if (votes >= 6){
            return "em em-slightly_smiling_face";
        } else if (votes >= 3){
            return "em em-neutral_face";
        } else if (votes >= 0){
            return "em em-confused";
        } else if (votes <= 0 && votes > -3){
            return "em em-angry";
        } else {
            return "em em-rage";
        }

    };

    /**
     * Replaces componentDidMount / componentDidUpdate.
     * @protected
     */
    useEffect(() => {

        setTimeout(() => {
            setShow(true);
        }, 1000);

    });

    return (

        <div className={show ? 'joke noselect visible' : "joke noselect"} >
            <div className="jokeButtons">
                <FontAwesomeIcon className="arrowIcon arrowUp" icon={faArrowUp} onClick={upVote} />
                <span className="jokeVotes" style={{ borderColor: getColor() }}>{votes}</span>
                <FontAwesomeIcon className="arrowIcon arrowDown" icon={faArrowDown} onClick={downVote} />
            </div>
            <div className="jokeText">{ text }</div>
            <div className="jokeEmoji">
                <i className={getEmoji()} />
            </div>
        </div>
        
    );

};

export default Joke;